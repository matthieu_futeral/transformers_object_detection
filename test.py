import argparse
from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
import numpy as np
import skimage.io as io
import pylab


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--RUN_ID', type=str, required=True)
    args = parser.parse_args()

    annType = 'bbox'
    annFile = 'data/coco/annotations/instances_val2017.json'
    cocoGt = COCO(annFile)

    pred_file = 'results/{}/{}.json'.format(args.RUN_ID, 'cocoDt')
    cocoDt = cocoGt.loadRes(pred_file)
    imgIds = sorted(cocoGt.getImgIds())

    # Start evaluate
    cocoEval = COCOeval(cocoGt, cocoDt, annType)
    cocoEval.params.imgIds = imgIds
    cocoEval.evaluate()
    cocoEval.accumulate()
    cocoEval.summarize()
