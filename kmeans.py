# -*- coding: utf-8 -*-

"""
Created on Tue Nov 10 19:47:39 2020

@author: matthieufuteral-peter
"""

import os
import torch
from utils.data import *
from utils.utils import bbox_anchor_iou
from tqdm import tqdm
import torch
from torch.utils.data import DataLoader


class kmeans:
    def __init__(self, bboxes, k, n_iter=500):

        self.bboxes = bboxes
        self.k = k
        self.n_iter = n_iter
        self.centroids = self.bboxes[:self.k, 2:4]
        self.count_lab = [0] * self.k

    def fit(self):
        for _ in tqdm(range(self.n_iter)):

            self.count_lab = [0] * self.k
            anchor_iou = torch.stack([bbox_anchor_iou(anchor, self.bboxes) for anchor in self.centroids])
            best_ious, best_n = anchor_iou.max(0)

            for n in range(self.k):
                clus = self.bboxes[best_n == n]
                self.centroids[n, 0], self.centroids[n, 1] = torch.mean(clus, dim=0)[2:]
                self.count_lab[n] = clus.size(0)


if __name__ == '__main__':

    # Look at some images
    path = os.getcwd()
    path_data = os.path.join(path, 'data')
    path_images = os.path.join(path_data, 'coco', 'images')
    path_anns = os.path.join(path_data, 'coco', 'annotations')

    # Loading data
    data_transformation = data_transform(size=384)
    dataset = loadDataset(path_root=path_images, path_ann=path_anns, dataset='train', transforms=data_transformation)
    print(len(dataset))
    train_dataset = DataLoader(dataset,
                               batch_size=64,
                               shuffle=True,
                               collate_fn=dataset.collate_fn)

    bboxes = []
    for _, (data, targets, area) in tqdm(enumerate(train_dataset)):
        bboxes.append(targets)

    bboxes = torch.cat(bboxes)[:, 2:] * 384

    km = kmeans(bboxes, 9)
    km.fit()
    print(km.centroids)
    print(km.count_lab)
