# -*- coding: utf-8 -*-

"""
Created on Tue Nov 10 19:47:39 2020

@author: matthieufuteral-peter
"""
import pdb
import os
import random
import numpy as np
from PIL import Image
import torch
import torchvision.transforms as T
import torchvision.datasets as datasets


def data_transform(size=384):
    output = []
    output.append(T.Resize((size, size)))
    output += [T.ToTensor(), T.Normalize(mean=[0.485, 0.456, 0.406],
                                         std=[0.229, 0.224, 0.225])]
    return T.Compose(output)


class loadDataset:
    def __init__(self, path_root, path_ann, dataset, im_size=384, transforms=None):

        self.path_root = path_root
        self.path_ann = path_ann
        self.dataset = dataset
        self.im_size = im_size
        self.batch_count = 0
        self.transforms = transforms

        assert self.dataset in ['train', 'val'], "dataset should be equal to train, val or test"
        assert not self.im_size % 32, "im_size should a multiple of 32"
        if not isinstance(self.transforms, T.Compose):
            raise TypeError("Transforms should be transforms.Compose type")

        try:
            self.data = datasets.CocoDetection(root=os.path.join(self.path_root, '{}2017'.format(self.dataset)),
                                               annFile=os.path.join(self.path_ann, 'instances_{}2017.json'.format(self.dataset)))
        except:
            raise ValueError("path root and/or path annFile incorrect")

        self.img_names = os.listdir(self.path_root + '/{}2017'.format(self.dataset))
        self.remove_empty()
        self.categories = self.data.coco.loadCats(self.data.coco.getCatIds())
        self.cat_names = {cat['id']: cat['name'] for cat in self.categories}
        self.id2cat = list(self.cat_names.values())
        self.cat2id = {v: k for k, v in enumerate(self.id2cat)}
        self.id2id = {k: i for i, k in enumerate(self.cat_names.keys())}

    def __getitem__(self, idx):

        id = self.ids[idx]
        annIds = self.data.coco.getAnnIds(imgIds=id)
        anns = self.data.coco.loadAnns(ids=annIds)
        # pdb.set_trace()

        img_info = self.data.coco.loadImgs(ids=id)
        img = Image.open(os.path.join(self.path_root,
                                      '{}2017'.format(self.dataset),
                                      img_info[0]['file_name'])).convert("RGB")
        bbox, area = self.extract_bbox(anns, self.id2id)
        bbox, ratio = self.transform_target(img, bbox, self.im_size)
        img_transformed = self.transforms(img)
        if random.random() < 0.5 and self.dataset == 'train':
            img_transformed = torch.flip(img_transformed, dims=[-1])
            bbox[:, 2] = 1 - bbox[:, 2]

        if self.dataset == 'train':
            return img_transformed, bbox, area
        elif self.dataset == 'val':
            return img_transformed, bbox, id

    def __len__(self):
        return len(self.ids)

    def collate_fn(self, batch):
        if self.dataset == 'train':
            imgs, bboxes, areas = list(zip(*batch))
        elif self.dataset == 'val':
            imgs, bboxes, img_id = list(zip(*batch))
        # Add sample index to bboxes
        for i, bb in enumerate(bboxes):
            bb[:, 0] = int(i)
        bboxes = torch.cat(bboxes, 0)
        self.batch_count += 1
        imgs = torch.stack(imgs)
        if self.dataset == 'train':
            return imgs, bboxes, areas
        else:
            return imgs, bboxes, img_id

    @staticmethod
    def extract_bbox(anns, id2id):
        bbox, area = [], []
        assert len(anns), "Annotations is empty when attempting extract bounding boxes"
        for ann in anns:
            x0, y0, w, h = ann['bbox'][0], ann['bbox'][1], ann['bbox'][2], ann['bbox'][3]
            lab = int(ann['category_id'])
            cat_lab = id2id[lab]
            bbox.append([x0, y0, w, h, cat_lab])
            area.append(ann['area'])
        return bbox, area

    @staticmethod
    def transform_target(img, bbox, im_size):
        '''
        (x center, y center, width, height)
        :param img:
        :param bbox:
        :param im_size:
        :return:
        '''
        ratio = (im_size / img.size[0], im_size / img.size[1])
        _bbox = []
        for bb in bbox:
            x0, y0 = (bb[0] + bb[2])/2, (bb[1] + bb[3])/2
            x0, w = (x0 * ratio[0])/im_size, (bb[2] * ratio[0])/im_size
            y0, h = (y0 * ratio[1])/im_size, (bb[3] * ratio[1])/im_size
            _bbox.append([0, bb[-1], x0, y0, w, h])
        return torch.Tensor(_bbox), ratio

    def remove_empty(self):
        self.ids = []
        for id in self.data.ids:
            annIds = self.data.coco.getAnnIds(imgIds=id)
            anns = self.data.coco.loadAnns(ids=annIds)
            img_info = self.data.coco.loadImgs(ids=id)
            if len(anns) and img_info[0]['file_name'] in self.img_names:
                self.ids.append(id)


def reverse_shape(dataset, bbox, im_size=384):
    img_info = dataset.data.coco.loadImgs(ids=bbox['image_id'])
    img = Image.open(os.path.join(dataset.path_root,
                                  '{}2017'.format(dataset.dataset),
                                  img_info[0]['file_name'])).convert("RGB")
    img = T.ToTensor()(img)
    ratio = (img.size(1) / im_size, img.size(2) / im_size)
    x0, w = bbox['bbox'][0] * ratio[0], bbox['bbox'][2] * ratio[0]
    y0, h = bbox['bbox'][1] * ratio[1], bbox['bbox'][3] * ratio[1]
    bbox['bbox'] = [x0, y0, w, h]
    return bbox
