# -*- coding: utf-8 -*-

"""
Created on Tue Nov 10 19:47:39 2020

@author: matthieufuteral-peter
"""

import pdb
import numpy as np
import torch
import torch.nn as nn
from tqdm import tqdm
from logger import logger


def logging(message, stdout):
    '''
    Print with logger the message and add it the output file stdout

    Parameters:
    message (str): message to print
    stdout (list): output file with messages printed

    Return:
    stdout (list): list of printed messages
    '''
    assert type(message) == str, "Message should be a string"
    logger.info(message)
    stdout.append(message)
    return stdout


def add_weight_decay(model, value):
    '''
    Add weight decay to model's parameters except biases

    Parameters:
        model (nn.Module pytorch): pytorch model
        value (float): value of the weight decay

    Return:
         list of params with no decay, params with decay
    '''
    decay, no_decay = [], []
    for name, param in model.named_parameters():
        if len(param.shape) == 1 or name.endswith(".bias"):
            no_decay.append(param)
        else:
            decay.append(param)
    return [{'params': no_decay, 'weight_decay': 0.}, {'params': decay, 'weight_decay': value}]



def xyxy2xywh(bbox):
    '''
    convert x_left y_left x_right y_right bbox format to x_center, y_center, width, height format
    :param bbox: numpy format (n_examples, bbox)
    :return: bbox numpy format (n_examples, bbox) with xywh format
    '''
    out = bbox.new(bbox.shape)
    out[:, 0], out[:, 1] = (bbox[:, 2] + bbox[:, 0]) / 2, (bbox[:, 3] + bbox[:, 1])/2  # x_center, y_center
    out[:, 2] = bbox[:, 2] - bbox[:, 0]  # width
    out[:, 3] = bbox[:, 3] - bbox[:, 1]  # height
    return out


def xywh2xyxy(bbox):
    '''
    convert x_left y_left x_right y_right bbox format from x_center, y_center, width, height format
    :param bbox: numpy format (n_examples, bbox)
    :return: bbox numpy format (n_examples, bbox) with xyxy format
    '''
    out = bbox.new(bbox.shape)
    out[..., 0], out[..., 1] = bbox[..., 0] - bbox[..., 2] / 2, bbox[..., 1] - bbox[..., 3] / 2  # x top left, y top left
    out[..., 2], out[..., 3] = bbox[..., 0] + bbox[..., 2] / 2, bbox[..., 1] + bbox[..., 3] / 2  # x bottom right, y bottom right
    return out


def area(B):
    '''
    compute the area of the bboxs B, format numpy (n examples, 4).
    :param B: bounding boxes format xyxy
    :return: area => numpy/torch (n examples, 1)
    '''
    return (B[:, 2] - B[:, 0]) * (B[:, 3] - B[:, 1])


def bbox_iou(A, B, xyxy=True):
    '''
    Compute the intersection over Union metric between bboxs A and bboxs B.
    :param A: bbox A => format tensor (n examples, 4) format xyxy if xyxy True else x_cy_cwh
    :param B: bbox B => format tensor (n examples, 4) format xyxy if xyxy True else x_cy_cwh
    :return: IoU between 0, 1
    '''
    assert A.shape == B.shape, "dim(A) does not match dim(B)"

    if not xyxy:
        A = xywh2xyxy(A)
        B = xywh2xyxy(B)

    if A.is_cuda:
        bb_inter = torch.zeros(A.shape).cuda()
    else:
        bb_inter = torch.zeros(A.shape)

    bb_inter[:, 0] = torch.maximum(A[:, 0], B[:, 0])
    bb_inter[:, 1] = torch.maximum(A[:, 1], B[:, 1])
    bb_inter[:, 2] = torch.minimum(A[:, 2], B[:, 2])
    bb_inter[:, 3] = torch.minimum(A[:, 3], B[:, 3])
    inter = area(bb_inter)
    return inter / (area(A) + area(B) - inter + 1e-16)


def bbox_iou_compare(A, B, xyxy=True):
    '''
    Compute the intersection over Union metric between bboxs A and bboxs B.
    :param A: bbox A => format tensor (n examples, 4) format xyxy if xyxy True else x_cy_cwh
    :param B: bbox B => format tensor (n examples, 4) format xyxy if xyxy True else x_cy_cwh
    :return: IoU between 0, 1
    '''

    if not xyxy:
        A = xywh2xyxy(A)
        B = xywh2xyxy(B)

    bb_inter = B.new(B.shape)
    bb_inter[:, 0] = torch.maximum(A[:, 0], B[:, 0])
    bb_inter[:, 1] = torch.maximum(A[:, 1], B[:, 1])
    bb_inter[:, 2] = torch.minimum(A[:, 2], B[:, 2])
    bb_inter[:, 3] = torch.minimum(A[:, 3], B[:, 3])
    inter = area(bb_inter)
    return inter / (area(A) + area(B) - inter + 1e-16)


def bbox_anchor_iou(anchor, bb):
    '''
    Computer IoU between anchor and bb
    :param anchor: (w, h) format
    :param bb: x_center, y_center, w, h format
    :return: IoU between anchor and bb
    '''
    # assert isinstance(anchor, tuple) and len(anchor) == 2, "Anchor should be a tuple of length 2"
    w, h = bb[:, -2], bb[:, -1]
    min_w, min_h = torch.minimum(anchor[0], w), torch.minimum(anchor[1], h)
    inter = min_w * min_h
    union = w * h + anchor[0] * anchor[1] - inter
    return inter / union


def process_target(pred_boxes, pred_cls, target, anchors, num_classes, grid_size, ignore_thres, device):

    ByteTensor = torch.cuda.ByteTensor if pred_boxes.is_cuda else torch.ByteTensor
    FloatTensor = torch.cuda.FloatTensor if pred_boxes.is_cuda else torch.FloatTensor

    batch_size = pred_boxes.size(0)
    num_anchors = len(anchors)

    # Output tensors
    obj_mask = ByteTensor(batch_size, num_anchors, grid_size, grid_size).fill_(0)
    noobj_mask = ByteTensor(batch_size, num_anchors, grid_size, grid_size).fill_(1)
    class_mask = FloatTensor(batch_size, num_anchors, grid_size, grid_size).fill_(0)
    iou_scores = FloatTensor(batch_size, num_anchors, grid_size, grid_size).fill_(0)
    tx = FloatTensor(batch_size, num_anchors, grid_size, grid_size).fill_(0)
    ty = FloatTensor(batch_size, num_anchors, grid_size, grid_size).fill_(0)
    tw = FloatTensor(batch_size, num_anchors, grid_size, grid_size).fill_(0)
    th = FloatTensor(batch_size, num_anchors, grid_size, grid_size).fill_(0)
    tcls = FloatTensor(batch_size, num_anchors, grid_size, grid_size, num_classes).fill_(0)

    # Convert to position relative to box
    target_boxes = target[:, 2:6] * grid_size
    # target from 0 to 1 and * grid_size to have similar scale with output network processing

    gxy = target_boxes[:, :2]
    gwh = target_boxes[:, 2:]
    # Get anchors with best iou
    anchor_iou = torch.stack([bbox_anchor_iou(anchor, target_boxes) for anchor in anchors])
    best_ious, best_n = anchor_iou.max(0)
    # Separate target values
    b, target_labels = target[:, :2].long().t()
    gx, gy = gxy.t()
    gw, gh = gwh.t()
    gi, gj = gxy.long().t()
    # Set masks
    obj_mask[b, best_n, gj, gi] = 1
    noobj_mask[b, best_n, gj, gi] = 0

    # Set noobj mask to zero where iou exceeds ignore threshold
    for i, anchor_ious in enumerate(anchor_iou.t()):
        noobj_mask[b[i], anchor_ious > ignore_thres, gj[i], gi[i]] = 0
        # Not best (anchor, target) iou but still greater than ignore_thres and we don't want to train the network to
        # predict no object for anchors with high iou with the target (even if it is not the max iou)

    # Coordinates
    tx[b, best_n, gj, gi] = gx - gx.floor()  # get tx between 0 and 1
    ty[b, best_n, gj, gi] = gy - gy.floor()  # get ty between 0 and 1
    # Width and height
    tw[b, best_n, gj, gi] = torch.log(gw / anchors[best_n][:, 0] + 1e-16)
    th[b, best_n, gj, gi] = torch.log(gh / anchors[best_n][:, 1] + 1e-16)
    # One-hot encoding of label
    tcls[b, best_n, gj, gi, target_labels] = 1
    # Compute label correctness and iou at best anchor
    class_mask[b, best_n, gj, gi] = (pred_cls[b, best_n, gj, gi].argmax(-1) == target_labels).float()
    iou_scores[b, best_n, gj, gi] = bbox_iou(pred_boxes[b, best_n, gj, gi], target_boxes, xyxy=False)

    tconf = obj_mask.float()
    obj_mask = obj_mask.long()
    return iou_scores, class_mask, obj_mask, noobj_mask, tx, ty, tw, th, tcls, tconf


def non_max_suppression(prediction, conf_thres=0.5, nms_thres=0.5):
    """
    Removes detections with lower object confidence score than 'conf_thres' and performs
    Non-Maximum Suppression to further filter detections.
    Returns detections with shape:
        (x1, y1, x2, y2, object_conf, class_score, class_pred)
    """
    # From (center x, center y, width, height) to (x1, y1, x2, y2)
    prediction[..., :4] = xywh2xyxy(prediction[..., :4])
    output = [None for _ in range(len(prediction))]
    for image_i, image_pred in enumerate(prediction):
        # Filter out confidence scores below threshold
        image_pred = image_pred[image_pred[:, 4] >= conf_thres]
        # If none are remaining => process next image
        if not image_pred.size(0):
            continue
        # Object confidence times class confidence
        score = image_pred[:, 4] * image_pred[:, 5:].max(1)[0]
        # Sort by it
        image_pred = image_pred[(-score).argsort()]
        class_confs, class_preds = image_pred[:, 5:].max(1, keepdim=True)
        detections = torch.cat((image_pred[:, :5], class_confs.float(), class_preds.float()), 1)
        # Perform non-maximum suppression
        keep_boxes = []
        while detections.size(0):
            large_overlap = bbox_iou_compare(detections[0, :4].unsqueeze(0), detections[:, :4]) > nms_thres
            label_match = detections[0, -1] == detections[:, -1]
            # Indices of boxes with lower confidence scores, large IOUs and matching labels
            invalid = large_overlap & label_match
            weights = detections[invalid, 4:5]
            # Merge overlapping bboxes by order of confidence
            detections[0, :4] = (weights * detections[invalid, :4]).sum(0) / weights.sum()
            keep_boxes += [detections[0]]
            detections = detections[~invalid]
        if keep_boxes:
            output[image_i] = torch.stack(keep_boxes)
    return output

