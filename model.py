# -*- coding: utf-8 -*-

"""
Created on Tue Nov 10 19:47:39 2020

@author: matthieufuteral-peter
"""

import os
import math
import timm
from timm.models.vision_transformer import Block
from utils.config import *
from utils.utils import *
import torch
import torch.nn as nn
import pdb


def create_modules(parsed_cfg):

    net_info = parsed_cfg[0]
    modules_model = nn.ModuleList()
    n_cls = 0
    n_yolo = 0
    n_split_pred = 0
    n_attention_blocks = 0

    for _, mods in enumerate(parsed_cfg[1:], 1):

        if mods['type'] == 'backbone':
            backbone = backbone_vit(cfg_vit=mods['version'], drop=float(mods['drop']))
            modules_model.add_module('backbone', backbone)

        elif mods['type'] == 'classifier':
            n_cls += 1
            cls_layer = classifier(embed_dim=int(mods['embed_dim']),
                                   output_dim=int(mods['output_dim']),
                                   bias=int(mods['bias']))
            modules_model.add_module('classifier{}'.format(n_cls), cls_layer)

        elif mods['type'] == 'yolo':
            grid_size = 12 * 2**n_yolo
            n_yolo += 1
            anchor_idxs = [int(x.strip()) for x in mods["mask"].split(",")]
            # Extract anchors
            anchors = [int(x.strip()) for x in mods["anchors"].split(",")]
            anchors = [(anchors[i], anchors[i + 1]) for i in range(0, len(anchors), 2)]
            anchors = [anchors[i] for i in anchor_idxs]
            num_classes = int(mods["classes"])
            img_size = int(net_info["height"])
            # Define detection layer
            yolo_layer = YOLOLayer(anchors=anchors, num_classes=num_classes, grid_size=grid_size, img_dim=img_size)
            modules_model.add_module("yolo{}".format(n_yolo), yolo_layer)

        elif mods['type'] == 'downscale':
            n_split_pred += 1
            embed_dim = int(mods['embed_dim'])
            dropout = float(mods['drop'])
            split_pred = downscale_layer(embed_dim=embed_dim, drop=dropout)
            modules_model.add_module('downscale{}'.format(n_split_pred), split_pred)

        elif mods['type'] == 'attention_blocks':
            n_attention_blocks += 1
            embed_dim = int(mods['embed_dim'])
            depth = int(mods['depth'])
            num_heads = int(mods['num_head'])
            dropout = float(mods['drop'])
            blocks = nn.Sequential()
            for n in range(depth):
                blocks.add_module('block{}'.format(n), Block(dim=embed_dim, num_heads=num_heads, drop=dropout))
            modules_model.add_module('Blocks{}'.format(n_attention_blocks), blocks)

    return net_info, modules_model

class classifier(nn.Module):
    def __init__(self, embed_dim, output_dim, bias):
        super(classifier, self).__init__()
        self.classifier = nn.Linear(embed_dim, output_dim, bias)
        self.layer_norm = nn.LayerNorm(output_dim)
        nn.init.xavier_uniform_(self.classifier.weight)
    def forward(self, x):
        return self.layer_norm(self.classifier(x))


class backbone_vit(nn.Module):
    '''
    Extract layers from ViT model (except last layers) in order to have a new backbone for Yolov3
    '''
    def __init__(self, cfg_vit, drop):
        super(backbone_vit, self).__init__()

        self.cfg_vit = cfg_vit
        self.backbone = timm.create_model(self.cfg_vit, pretrained=True, drop_rate=drop)

    def forward(self, x):
        B = x.shape[0]
        x = self.backbone.patch_embed(x)

        cls_tokens = self.backbone.cls_token.expand(B, -1, -1)
        x = torch.cat((cls_tokens, x), dim=1)
        x = x + self.backbone.pos_embed
        x = self.backbone.pos_drop(x)

        for blk in self.backbone.blocks:
            x = blk(x)

        x = self.backbone.norm(x)
        return x[:, 1:]


class downscale_layer(nn.Module):
    '''
    Change predictions scale according to Yolov3
    '''
    def __init__(self, embed_dim, drop):
        super(downscale_layer, self).__init__()

        self.linear = nn.Linear(embed_dim, embed_dim * 4, bias=False)
        nn.init.xavier_uniform_(self.linear.weight)
        self.drop = nn.Dropout(p=drop)
        self.embed_dim = embed_dim

    def forward(self, x):
        x = self.drop(x)
        out = self.linear(x).view(x.size(0), -1, self.embed_dim)
        return out


class YOLOLayer(nn.Module):
    """Detection layer"""

    def __init__(self, anchors, num_classes, grid_size, img_dim=416):
        super(YOLOLayer, self).__init__()
        self.anchors = anchors
        self.num_anchors = len(anchors)
        self.num_classes = num_classes
        self.ignore_thres = 0.5
        self.mse_loss = nn.MSELoss()
        self.bce_loss = nn.BCELoss()
        self.obj_scale = 1
        self.noobj_scale = 100
        self.metrics = {}
        self.img_dim = img_dim
        self.grid_size = grid_size  # grid size
        self.stride = img_dim // grid_size

    def compute_grid_offsets(self, grid_size, cuda=torch.cuda.is_available()):
        self.grid_size = grid_size
        g = self.grid_size
        FloatTensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor
        self.stride = self.img_dim / self.grid_size
        # Calculate offsets for each grid
        self.grid_x = torch.arange(g).repeat(g, 1).view([1, 1, g, g]).type(FloatTensor)
        self.grid_y = torch.arange(g).repeat(g, 1).t().view([1, 1, g, g]).type(FloatTensor)
        self.scaled_anchors = FloatTensor([(a_w / self.stride, a_h / self.stride) for a_w, a_h in self.anchors])
        self.anchor_w = self.scaled_anchors[:, 0:1].view((1, self.num_anchors, 1, 1))
        self.anchor_h = self.scaled_anchors[:, 1:2].view((1, self.num_anchors, 1, 1))

    def forward(self, x, targets=None):

        # Tensors for cuda support
        FloatTensor = torch.cuda.FloatTensor if x.is_cuda else torch.FloatTensor
        LongTensor = torch.cuda.LongTensor if x.is_cuda else torch.LongTensor
        ByteTensor = torch.cuda.ByteTensor if x.is_cuda else torch.ByteTensor

        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        num_samples = x.size(0)
        grid_size = self.grid_size

        prediction = (
            x.view(num_samples, self.num_anchors, self.num_classes + 5, grid_size, grid_size)
            .permute(0, 1, 3, 4, 2)
            .contiguous()
        )

        # Get outputs
        x = torch.sigmoid(prediction[..., 0])  # Center x
        y = torch.sigmoid(prediction[..., 1])  # Center y
        w = prediction[..., 2]  # Width
        h = prediction[..., 3]  # Height
        pred_conf = torch.sigmoid(prediction[..., 4])  # Conf
        pred_cls = torch.sigmoid(prediction[..., 5:])  # Cls pred.

        # If grid size does not match current we compute new offsets
        self.compute_grid_offsets(grid_size, cuda=x.is_cuda)

        # Add offset and scale with anchors
        pred_boxes = FloatTensor(prediction[..., :4].shape).to(device)
        pred_boxes[..., 0] = x.data + self.grid_x
        pred_boxes[..., 1] = y.data + self.grid_y
        pred_boxes[..., 2] = torch.exp(w.data) * self.anchor_w
        pred_boxes[..., 3] = torch.exp(h.data) * self.anchor_h

        output = torch.cat(
            (
                pred_boxes.view(num_samples, -1, 4) * self.stride,
                pred_conf.view(num_samples, -1, 1),
                pred_cls.view(num_samples, -1, self.num_classes),
            ),
            -1,
        )

        # pdb.set_trace()

        if targets is None:
            return output, 0
        else:
            iou_scores, class_mask, obj_mask, noobj_mask, tx, ty, tw, th, tcls, tconf = process_target(
                pred_boxes=pred_boxes,
                pred_cls=pred_cls,
                grid_size=self.grid_size,
                target=targets,
                num_classes=self.num_classes,
                anchors=self.scaled_anchors,
                ignore_thres=self.ignore_thres,
                device=device
            )

            # Loss : Mask outputs to ignore non-existing objects (except with conf. loss)
            loss_x = self.mse_loss(x[obj_mask == 1], tx[obj_mask == 1])
            loss_y = self.mse_loss(y[obj_mask == 1], ty[obj_mask == 1])
            loss_w = self.mse_loss(w[obj_mask == 1], tw[obj_mask == 1])
            loss_h = self.mse_loss(h[obj_mask == 1], th[obj_mask == 1])
            loss_conf_obj = self.bce_loss(pred_conf[obj_mask == 1], tconf[obj_mask == 1])
            loss_conf_noobj = self.bce_loss(pred_conf[noobj_mask == 1], tconf[noobj_mask == 1])
            loss_conf = self.obj_scale * loss_conf_obj + self.noobj_scale * loss_conf_noobj
            loss_cls = self.bce_loss(pred_cls[obj_mask == 1], tcls[obj_mask == 1])
            total_loss = loss_x + loss_y + loss_w + loss_h + loss_conf + loss_cls

            # Metrics
            cls_acc = 100 * class_mask[obj_mask == 1].mean()
            conf_obj = pred_conf[obj_mask == 1].mean()
            conf_noobj = pred_conf[noobj_mask == 1].mean()
            conf50 = (pred_conf > 0.5).float()
            iou50 = (iou_scores > 0.5).float()
            iou75 = (iou_scores > 0.75).float()
            detected_mask = conf50 * class_mask * tconf
            precision = torch.sum(iou50 * detected_mask) / (conf50.sum() + 1e-16)
            recall50 = torch.sum(iou50 * detected_mask) / (obj_mask.sum() + 1e-16)
            recall75 = torch.sum(iou75 * detected_mask) / (obj_mask.sum() + 1e-16)

            self.metrics = {
                "loss": total_loss.item(),
                "x": loss_x.item(),
                "y": loss_y.item(),
                "w": loss_w.item(),
                "h": loss_h.item(),
                "conf": loss_conf.item(),
                "cls": loss_cls.item(),
                "cls_acc": cls_acc.item(),
                "recall50": recall50.item(),
                "recall75": recall75.item(),
                "precision": precision.item(),
                "conf_obj": conf_obj.item(),
                "conf_noobj": conf_noobj.item(),
                "grid_size": self.grid_size,
            }

            return output, total_loss


class Yolo_vit(nn.Module):
    def __init__(self, cfg):
        super(Yolo_vit, self).__init__()

        path_cfg = os.path.join('cfg', cfg)
        self.model_info = parse_config(path_cfg)
        self.network_info, self.model = create_modules(self.model_info)

    def forward(self, x, targets=None):

        modules = self.model_info[1:]
        pred, loss = [], []
        for i, mods in enumerate(self.model):
            module_type = modules[i]['type']

            if module_type in ['backbone', 'attention_blocks']:
                x = mods(x)
                output = x.clone()

            elif module_type == 'classifier':
                x = mods(x)

            elif module_type == 'yolo':
                x, total_loss = mods(x=x, targets=targets)
                pred.append(x)
                loss.append(total_loss)

            elif module_type == 'downscale':
                x = mods(output)
        if targets is None:
            return torch.cat(pred, 1)
        else:
            return torch.cat(pred, 1), loss
