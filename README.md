# Vision Transformers for Object Detection

Implementation of a Transformer-based Yolov3. The general architecture is illustrated in the figure below :
<p align="center">
<img src="./img/vit_model.png"  width="640" height="360">
</p>

## Requirements
```console
$ pip3 install -r requirements.txt
```


## Data
To download [MS Coco dataset 2017](https://cocodataset.org/#download), please run the following command line :
```sh
$ sh get_coco_dataset.sh
```


## Usage

- To train a model :
```console
python3 main.py --mode train --cfg yolo_vit_large32_384_large.cfg --batch_size 64 --epochs 100 \
--lr 1e-3 --debug 0 --weight_decay 1e-4 --log_interval 100 --patience 7 --horizontal_flip 1 \
--accumulation_steps 1 --conf_thres .5 --nms_thres .5
```

- To do inference :
```console
python3 main.py --mode test --cfg yolo_vit_large32_384_large.cfg --batch_size 64 --debug 0 \
--log_interval 100 --conf_thres .5 --nms_thres .5 --RUN_ID 5a539
```
