# -*- coding: utf-8 -*-

"""
Created on Sat Nov 7 12:47:39 2020

@author: matthieufuteral-peter
"""

import os
import time
import json
import pdb
import argparse
from utils.data import *
from utils.utils import *
from model import *
import torch
import torch.optim as optim
from torch.utils.data import DataLoader
from uuid import uuid4
from early_stopping_pytorch.pytorchtools import EarlyStopping


def train(model, epoch, batch_size, train_loader, use_cuda, log_interval, train_loss, stdout, accumulation_steps):

    optimizer.zero_grad()
    model.train()
    train_batch_loss = []

    for batch_idx, (data, target, area) in tqdm(enumerate(train_loader)):

        if use_cuda:
            data, target = data.cuda(), target.cuda()

        output, loss = model(data, target)
        loss = torch.sum(torch.Tensor(loss))

        if not (batch_idx + 1) % accumulation_steps:  # Gradient accumulation to handle limit of GPU RAM
            optimizer.step()
            optimizer.zero_grad()

        if not (batch_idx + 1) % log_interval:
            logger.info('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), np.mean(train_batch_loss)))

        train_batch_loss.append(loss.data.item() / batch_size)

        del data, target  # free space on gpu
        torch.cuda.empty_cache()

    train_loss.append(np.mean(train_batch_loss))
    stdout = logging('Train loss Epoch {} : {}'.format(epoch, train_loss[-1]), stdout)

    return model, train_loss, stdout


def validate(model, epoch, val_loader, use_cuda, val_loss, stdout, conf_thres, nms_thres, mode):

    out = []
    model.eval()
    validation_loss = [0, 0, 0]
    with torch.no_grad():
        for data, target, img_id in tqdm(val_loader):

            if use_cuda:
                data, target = data.cuda(), target.cuda()

            output, loss = model(data, target)
            for i in range(len(validation_loss)):
                validation_loss[i] += loss[i].data.item()
            if mode == 'test':
                nms_out = non_max_suppression(output, conf_thres, nms_thres)
                for j, bboxes in enumerate(nms_out):
                    for bb in bboxes:
                        out.append({"image_id": int(img_id[j]),
                                    "category_id": int(bb[-1].data.item()),
                                    "bbox": [bb[i].data.item() for i in range(4)],
                                    "score": bb[4].data.item() * bb[5].data.item()})

            del data, target  # leave space on GPU
            torch.cuda.empty_cache()

    validation_loss = [v_loss / len(val_loader.dataset) for v_loss in validation_loss]
    stdout = logging('\nValidation set - Epoch {} : Average loss (L): {:.4f} '
                     '- Average loss (M): {:.4f} - Average loss (S): {:.4f}'.format(epoch, *validation_loss), stdout)

    val_loss.append(validation_loss)

    if mode == 'train':
        return val_loss, stdout
    else:
        return out


def main(model, epochs, batch_size, train_loader, val_loader, use_cuda, log_interval, scheduler,
         early_stopping, stdout, accumulation_steps, conf_thres, nms_thres):

    train_loss, val_loss, epoch_time = [], [], []

    for epoch in range(1, epochs + 1):
        stdout = logging("Epoch {} - Start TRAINING".format(epoch), stdout)
        t0 = time.time()
        # Training for one epoch
        model, train_loss, stdout = train(model, epoch, batch_size, train_loader, use_cuda, log_interval, train_loss,
                                          stdout, accumulation_steps)

        # Testing mode - test on the validation set
        val_loss, stdout = validate(model, epoch, val_loader, use_cuda, val_loss, stdout, conf_thres, nms_thres,
                                    mode='train')

        # Update learning rate
        if not epoch % 2:
            scheduler.step()

        # Early stopping
        early_stopping(val_loss[-1], model, stdout)
        if early_stopping.early_stop:
            message = "Early stopping"
            print(message)
            stdout.append(message)
            break

        # Save time elapsed
        time_elapsed = time.time() - t0
        stdout = logging("Epoch {} - Time elapsed : {}".format(epoch, time_elapsed), stdout)
        epoch_time.append(time_elapsed)
        stdout.append(' ')

        torch.cuda.empty_cache()  # Free memory

    stdout = logging("Average time per epoch : {}".format(np.mean(epoch_time)), stdout)
    stdout.append(' ')

    return model, train_loss, val_loss, epoch_time, stdout


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--mode', type=str, default='train', help='train or test mode')
    parser.add_argument('--debug', type=int, default=0)
    parser.add_argument('--cfg', type=str, default='yolo_vit_large32_384_large.cfg',
                        help='config yolo transformer model')
    parser.add_argument('--batch_size', type=int, default=64, metavar='B',
                        help='input batch size for training (default: 64)')
    parser.add_argument('--epochs', type=int, default=100, metavar='N',
                        help='number of epochs to train (default: 10)')
    parser.add_argument('--lr', type=float, default=1e-4, metavar='LR',
                        help='learning rate (default: 0.01)')
    parser.add_argument('--weight_decay', type=float, default=1e-4, help='weight decay ADAM optimizer')
    parser.add_argument('--log_interval', type=int, default=100, metavar='N',
                        help='how many batches to wait before logging training status')
    parser.add_argument('--patience', type=int, default=7, help="Early stopping patience")
    parser.add_argument('--horizontal_flip', type=int, default=1, help="perform hor. flip data augmentation")
    parser.add_argument('--accumulation_steps', type=int, default=1, help="Gradient accumulation for GPU RAM")
    parser.add_argument('--conf_thres', type=float, default=0.5, help='confidence threshold for NMS algorithm')
    parser.add_argument('--nms_thres', type=float, default=0.5, help='Non maximum suppression threshold')
    parser.add_argument('--checkpoint', type=int, default=0, help='Load checkpoint model to continue training')
    parser.add_argument('--RUN_ID', type=str, help='Checkpoint model run id')
    args = parser.parse_args()
    use_cuda = torch.cuda.is_available()
    torch.manual_seed(42)

    if not args.debug:
        pdb.set_trace = lambda: None

    stdout = []  # Save stdout file
    RUN_ID = str(uuid4())[0:5]
    print('RUN_ID : {}'.format(RUN_ID))
    device = torch.device("cuda" if use_cuda else "cpu")

    path = os.getcwd()
    path_data = os.path.join(path, 'data')
    path_images = os.path.join(path_data, 'coco', 'images')
    path_anns = os.path.join(path_data, 'coco', 'annotations')
    path_train = os.path.join(path_images, 'train2017')
    path_val = os.path.join(path_images, 'val2017')
    val_annFile = os.path.join(path_anns, 'instances_val2017.json')
    train_annFile = os.path.join(path_anns, 'instances_train2017.json')

    # Loading data
    data_transformation = data_transform(size=384)
    #train_dataset = loadDataset(path_root=path_images, path_ann=path_anns, dataset='train',
    #                            transforms=data_transformation)
    val_dataset = loadDataset(path_root=path_images, path_ann=path_anns, dataset='val', transforms=data_transformation)
    #train_loader = DataLoader(train_dataset,
    #                          batch_size=args.batch_size,
    #                          shuffle=True,
    #                          collate_fn=train_dataset.collate_fn)
    val_loader = DataLoader(val_dataset,
                            batch_size=args.batch_size,
                            shuffle=True,
                            collate_fn=val_dataset.collate_fn)

    # Load model
    model = Yolo_vit(cfg=args.cfg)
    if use_cuda:
        model.to(device)
    if args.checkpoint:
        weights = torch.load('results/{}/model.pt'.format(args.RUN_ID), map_location=device)
        model.load_state_dict(weights)
        print("Checkpoint weights loaded")

    if use_cuda:
        print(torch.cuda.get_device_name(0))
        stdout.append(torch.cuda.get_device_name(0))
    else:
        print('Using CPU')

    if args.mode == 'train':
        # Create folder results
        path_result = os.path.join('results', RUN_ID)
        if not os.path.isdir(path_result):
            os.makedirs(path_result)
            results = {'RUN_ID': RUN_ID,
                       'cfg': args.cfg,
                       'batch_size': args.batch_size,
                       'epochs': args.epochs,
                       'lr': args.lr,
                       "patience": args.patience,
                       "weight_decay": args.weight_decay,
                       "scheduler": "CosineAnnealing",
                       "horizontal_flip": args.horizontal_flip,
                       "confidence_threshold": args.conf_thres,
                       "nms_threshold": args.nms_thres}

            stdout += ['{} : {}'.format(str(k), str(v)) for k, v in results.items()]
            stdout.append(' ')

        # Optimizer
        params = add_weight_decay(model, args.weight_decay)
        optimizer = optim.Adam(params, lr=args.lr)

        # Scheduler
        lambda_schedule = lambda epoch: 0.95 ** (epoch // 2)
        scheduler = optim.lr_scheduler.LambdaLR(optimizer=optimizer, lr_lambda=lambda_schedule)

        # Set up early stopping
        early_stopping = EarlyStopping(patience=args.patience, verbose=True, path=os.path.join(path_result, 'model.pt'))

        # Summary Writer - Tensorboard
        path_report = os.path.join(path_result, 'report')
        model, train_loss, val_loss, epoch_time, stdout = main(model, args.epochs, args.batch_size, train_loader,
                                                               val_loader, use_cuda, args.log_interval, scheduler,
                                                               early_stopping, stdout, args.accumulation_steps,
                                                               args.conf_thres, args.nms_thres)

        results['train_loss'] = train_loss
        results['val_loss'] = val_loss
        results['epoch_time'] = epoch_time

        # Saving results
        with open(os.path.join(path_result, 'results.json'), 'w') as fj:
            json.dump(results, fj)

        stdout = logging("End of training - save stdout file", stdout)
        with open(os.path.join(path_result, 'stdout.txt'), 'w') as f:
            f.write('\n'.join(stdout))

        pdb.set_trace()

    elif args.mode == 'test':

        path_model = os.path.join(path, 'results', args.RUN_ID, 'model.pt')
        path_save_results = os.path.join(path, 'results', args.RUN_ID, 'submission.json')

        logger.info('Start loading model...')
        model = Yolo_vit(cfg=args.cfg)
        model.to(device)
        state_dict = torch.load(path_model, map_location=device)
        model.load_state_dict(state_dict)
        model.eval()

        logger.info('Model loaded... Start evaluation on COCO validation set 2017...')
        submission = validate(model, 1, val_loader, use_cuda, [0], stdout, args.conf_thres, args.nms_thres, mode='test')
        with open(path_save_results, 'w') as fj:
            json.dump(submission, fj)


    else:
        raise NameError("mode should be train or test - please check your entry")
